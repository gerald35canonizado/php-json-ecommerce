	<!-- NAVBAR -->
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
	  	<a class="navbar-brand" href="#">Sounds Better?</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor02" aria-controls="navbarColor02" aria-expanded="false" aria-label="Toggle navigation">
		    <span class="navbar-toggler-icon"></span>
	 	</button>

		<div class="collapse navbar-collapse" id="navbarColor02">
			<ul class="navbar-nav mr-auto">
				<li class="nav-item active">
					<a class="nav-link" href="../views/catalogue.php">Catalogue</a>
				</li>
				<?php
					session_start();
					if (isset($_SESSION['name'])) {
						if ($_SESSION['email']=="admin@admin.com") {
						?>
						<li class="nav-item">
						<a class="nav-link" href="../views/add_product.php">Add Product</a>
						</li>
						<?php
						}else{
					?>
						<li class="nav-item">
							<a class="nav-link" href="../views/cart.php">Show Cart</a>
						</li>					
					<?php		
						}
					?>
						<li class="nav-item">
							<a class="nav-link" href="#">Hello, <?php echo $_SESSION['name']?></a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="../controllers/process_logout.php">Log Out</a>
						</li>
					<?php
					} else {
					?>
						<li class="nav-item">
						<a class="nav-link" href="../views/login.php">Log In</a>
						</li>
						<li class="nav-item">
						<a class="nav-link" href="../views/register.php">Register</a>
						</li>
					<?php
					}
				?>
			</ul>
		<form class="form-inline my-2 my-lg-0">
			<input class="form-control mr-sm-2" type="text" placeholder="Search">
			<button class="btn btn-secondary my-2 my-sm-0" type="submit">Search</button>
	    </form>
	  	</div>
	</nav>