<?php
	require "../templates/templates.php";

	function get_content(){
?>
	<h1 class="text-center py-4">CATALOGUE</h1>
	<div class="container">
		<div class="row">
			<?php
				// "file_get_contents" getting the content of the JSON FILE
				$products = file_get_contents("../assets/lib/products.json");
				// make "$products" into an associative array
				$products_array = json_decode($products, true);

				foreach ($products_array as $indiv_product) {
				?>
				<div class="col-lg-4 py-2">
					<div class="card">
						<!-- publishing the image in the "products.json" -->
						<img class="card-img-top" height="300px" 
						src="../assets/lib/<?php echo $indiv_product['image'] ?>">
						<div class="card-body">
							<h5 class="card-title"><?php echo $indiv_product['name'] ?></h5>
							<p class="card-text">Price : <?php echo $indiv_product['price'] ?></p>
							<p class="card-text">Description : <?php echo $indiv_product['description'] ?></p>
						</div>
						<?php
							if (isset($_SESSION['email']) && $_SESSION['email'] == "admin@admin.com") {
						?>
						<div class="footer">
							<!-- delete the product using the "name" -->
							<a href="../controllers/process_delete_product.php?name=<?php echo $indiv_product['name'] ?>"class="btn btn-danger">Delete Product</a>
						</div>
						<?php
							} else {
						?>

						<div class="card-footer">
							<form action="../controllers/process_add_cart.php" method="POST">
								<input type="hidden" name="name" value="<?php echo $indiv_product['name']?>">
								<input type="number" name="quantity" value="1" class="form-control">
								<button type="submit" class="btn btn-info">Add to Cart</button>
							</form>							
						</div>
						<?php
							}
						?>
					</div>
				</div>
				<?php
				}
			?>
		</div>
	</div>
<?php		
	}
?>