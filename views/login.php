<?php
	require "../templates/templates.php";

	function get_content(){
?>
	<h1 class="text-center py4">Log In</h1>
	<div class="col-lg-4 offset-lg-4">
		<form action="../controllers/process_login.php" method="POST">
			<div class="form-group">
				<label for="email">Email</label>
				<input type="email" name="email" class="form-control">
			</div>
			<div class="form-group">
				<label for="password">Password</label>
				<input type="password" name="password" class="form-control">
			</div>
			<button class="btn btn-warning" type="submit">Log In</button>
		</form>		
	</div>
<?php
}
?>