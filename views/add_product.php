<?php
	require "../templates/templates.php";

	function get_content(){
?>
	<h1 class="text-center py-4">Add Product</h1>
	<div class="container col-lg-6 offset-lg-3">
		<form action="../controllers/process_add_product.php" method="POST" enctype="multipart/form-data">
			<div class="form-group">
				<label for="name">Model Name</label>
				<input type="text" name="name" class="form-control">
			</div>
			<div class="form-group">
				<label for="price">Price</label>
				<input type="number" name="price" class="form-control">
			</div>
			<div class="form-group">
				<label for="description">Description</label>
				<textarea name="description" class="form-control"></textarea>
			</div>
			<div class="form-group">
				<label for="image">Image</label>
				<!-- type="file" - upload file -->
				<input type="file" name="image" class="form-control">
			</div>
			<button class="btn btn-success" type="submit">Add Product</button>
		</form>		
	</div>
<?php
	}
?>