<?php
	require "../templates/templates.php";

	function get_content(){
?>
	<h1 class="text-center py-4">Register</h1>
	<div class="container">
		<div class="row">
			<div class="col-lg-6 offset-lg-3">
				<form action="../controllers/process_register.php" method="POST">
					<div class="form-group">
						<label for="firstName">First Name</label>						
						<input type="text" name="firstName" class="form-control">
						<label for="lastName">Last Name</label>						
						<input type="text" name="lastName" class="form-control">
					</div>
					<div class="form-group">
						<label for="email">Email</label>
						<input type="email" name="email" class="form-control">
					</div>
					<div class="form-group">
						<label for="password">Password</label>
						<input type="password" name="password" class="form-control">
					</div>
					<button class="btn btn-secondary" type="submit">Register</button>
				</form>				
			</div>			
		</div>		
	</div>
<?php
}
?>