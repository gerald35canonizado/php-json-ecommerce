<?php
	require "../templates/templates.php";

	function get_content(){
?>
	<h1 class="text-center py-4">Cart Page</h1>
	<div class="container">
		<div class="row">
			<div class="col-lg-8 offset-lg-2">
				<table class="table table-striped">
					<thead>
						<th>Product Name :</th>
						<th>Price :</th>
						<th>Quantity</th>
						<th>Subtotal</th>
					</thead>
					<tbody>
						<?php
							$products = file_get_contents("../assets/lib/products.json");
							$products_array = json_decode($products, true);
							$total = 0;

							// check if session exists
							if(isset($_SESSION['cart'])) {
							// if session['cart'] exist loop through the array getting the name and the quantity
							foreach ($_SESSION['cart'] as $name => $quantity) {
								// loop through our $products_array to get the invidual product information
								foreach ($products_array as $indiv_product) {
								// check if $name (from $_SESSION['cart'] is equal to indiv_product['name'])
									if ($name==$indiv_product['name']) {
										$subtotal = $quantity*$indiv_product['price'];
										$total += $subtotal;
										?>
										<tr>
											<td><?php echo $indiv_product['name']?></td>
											<td><?php echo $indiv_product['price']?></td>
											<td><?php echo $quantity ?></td>
											<td>PHP <?php echo $subtotal ?>.00</td>
											<td><a href="../controllers/process_remove_item.php?name=<?php echo $indiv_product['name'] ?>"class"btn btn-danger">Remove Item</a></td>
										</tr>
										<?php
										}
									}								
								}
							}
						?>
						<tr>
							<td></td>
							<td></td>
							<td><a class="btn btn-danger" href="../controllers/process_empty_cart.php">Empty Cart</a></td>
							<td>Total : PHP<?php echo $total ?>.00</td>
						</tr>
					</tbody>
				</table>
			</div>
			
		</div>
		
	</div>
<?php
	}

?>