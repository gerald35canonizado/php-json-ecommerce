<?php
	$name = $_POST['name'];
	$price = intval($_POST['price']);
	$description = $_POST['description'];

	// get the image property
	// "$_FILES" save to global variable
	// getting the file name of the added file
	$filename = $_FILES['image']['name'];
	//getting the file size of the added file
	$filesize = $_FILES['image']['size'];
	$file_tmpname = $_FILES['image']['tmp_name'];
	//getting the file type of the added file
	//strtolower(str) transfer to lower case
	$file_type = strtolower(pathinfo($filename, PATHINFO_EXTENSION));

	//					VALIDATION
	$hasDetails = false;
	$isImg = false;

	if ($name != "" && $price > 0 && $description != "") {
		$hasDetails = true;
	};

	if ($file_type == "jpg" || $file_type == "jpeg" ||  $file_type == "png") {
		$isImg = true;
	};

	// validating and moving the uploaded file to images library
	if ($filesize>0 && $isImg == true && $hasDetails == true) {
		$final_path = "../assets/lib/images/" . $filename;

		// moving the uploaded file (filename + pasting directory)
		move_uploaded_file($file_tmpname, $final_path);
	} else {
		echo "Invalid! Please Try Again";
	};

	$image = "images/" . $filename;
	$newProduct = [
		"name" => $name,
		"price" => $price,
		"description" => $description,
		"image" => $image
	];

	$json = file_get_contents("../assets/lib/products.json");
	$products = json_decode($json, true);
	array_push($products, $newProduct);

	// write the new array to our products.json
	$to_write = fopen("../assets/lib/products.json", "w");

	fwrite($to_write, json_encode($products, JSON_PRETTY_PRINT));
	fclose($to_write);
	header("Location: ../views/catalogue.php");
?>