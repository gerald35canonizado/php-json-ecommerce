<?php
	//$_GET if we delete via URL
	$name = $_GET['name'];
	// getting the contents of the file
	$products = file_get_contents("../assets/lib/products.json");
	// transform to associative array
	$products_array = json_decode($products, true);

	foreach($products_array as $index=>$product){
		if ($name==$product['name']){
			// unset - to remove
			unset($products_array[$index]);
		};
	};

	$to_write = fopen("../assets/lib/products.json", 'w');
	fwrite($to_write, json_encode($products_array, JSON_PRETTY_PRINT));
	fclose($to_write);
	// to redirect back to the page where it came from
	// $_SERVER
	header("Location: " . $_SERVER['HTTP_REFERER']);
?>