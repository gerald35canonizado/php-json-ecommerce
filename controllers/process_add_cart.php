<?php
	// allow us to use $_SESSION
	session_start();
	$name = $_POST['name'];
	$quantity = $_POST['quantity'];

	// if first time create session cart->keyName product name
	// isset - function that returns true if the variable inside exists
	if (!isset($_SESSION['cart'][$name])) {
		$_SESSION['cart'][$name] = $quantity;
	} else {
		$_SESSION['cart'][$name] += $quantity;
	}
?>