<?php
	session_start();
	// remove all sessions
	session_unset();

	// session destroy
	session_destroy();

	header("Location: ../views/catalogue.php");
?>