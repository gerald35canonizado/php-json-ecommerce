<?php
	$firstName = $_POST['firstName'];
	$lastName = $_POST['lastName'];
	$email = $_POST['email'];
	$password = sha1($_POST['password']);

	$newUser = [
		"firstName" => $firstName,
		"lastName" => $lastName,
		"email" => $email,
		"password" => $password
	];

	if($firstName !== "" && $lastName !== "" && $email !== "" && $password !== "") {
		$json = file_get_contents("../assets/lib/users.json");
		$users = json_decode($json, true);
		array_push($users, $newUser);
		
		$to_write = fopen("../assets/lib/users.json", "w");
		fwrite($to_write, json_encode($users, JSON_PRETTY_PRINT));
		fclose($to_write);

		header("Location: ../views/login.php");
	} else {
		echo "Please fill out the form properly";
	};
?>